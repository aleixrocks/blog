FROM ruby:2.7

# Install and setup required packages
RUN apt-get update
RUN apt-get install -y python3-pip
RUN pip3 install docutils pygments
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 10

#RUN gem install bundler # for now, let's use the old version
RUN gem install bundler
ENV JEKYLL_ENV=production
ENV LC_ALL=C.UTF-8

# Move to temporal direcotry and install gems
WORKDIR /tmp
COPY Gemfile Gemfile.lock ./
RUN bundle install

# move to app's directory and copy the blog files
WORKDIR /usr/src/app
COPY . .

# open containers' port 4000 and run app
EXPOSE 4000
CMD [ "bundle", "exec", "jekyll", "serve", "--drafts"]
