---
layout: page
title: About
permalink: /about/
---

I'm a PhD student at the Barcelona Supercomputing Center (BSC) on the
interaction between runtime systems and the Linux Kernel. While diving into the
Linux kernel internals, I thought in sharing some of my findings here hoping it
will be useful for others too :-)

Happy coding!
