---
layout: post
title:  "Understanding the Linux CFS"
date:   2020-05-23 00:32:14 -0300
categories: jekyll update
---

=================
Basics on CFS
=================
-------------------------
Clever subtitle goes here
-------------------------

Introduction
============

Summary from: https://www.kernel.org/doc/html/latest/scheduler/sched-design-CFS.html

In practice, the virtual runtime of a task is its actual runtime normalized to the total number of running tasks.

In CFS the virtual runtime is expressed and tracked via the per-task p->se.vruntime (nanosec-unit) value

CFS’s task picking logic is based on this p->se.vruntime value and it is thus very simple: it always tries to run the task with the smallest p->se.vruntime value (i.e., the task which executed least so far)

CFS also maintains the rq->cfs.min_vruntime value, which is a monotonic increasing value tracking the smallest vruntime among all tasks in the runqueue

The total number of running tasks in the runqueue is accounted through the rq->cfs.load value, which is the sum of the weights of the tasks queued on the runqueue.

CFS maintains a time-ordered rbtree, where all runnable tasks are sorted by the p->se.vruntime key. CFS picks the “leftmost” task from this tree and sticks to it. As the system progresses forwards, the executed tasks are put into the tree more and more to the right — slowly but surely giving a chance for every task to become the “leftmost task” and thus get on the CPU within a deterministic amount of time.

Summing up, CFS works like this: it runs a task a bit, and when the task schedules (or a scheduler tick happens) the task’s CPU usage is “accounted for”: the (small) time it just spent using the physical CPU is added to p->se.vruntime. Once p->se.vruntime gets high enough so that another task becomes the “leftmost task” of the time-ordered rbtree it maintains (plus a small amount of “granularity” distance relative to the leftmost task so that we do not over-schedule tasks and trash the cache), then the new leftmost task is picked and the current task is preempted.

you have to switch on CONFIG_SCHED_DEBUG): /proc/sys/kernel/sched_min_granularity_ns

CFS implements three scheduling policies:

SCHED_NORMAL (traditionally called SCHED_OTHER): The scheduling policy that is used for regular tasks.
SCHED_BATCH: Does not preempt nearly as often as regular tasks would, thereby allowing tasks to run longer and make better use of caches but at the cost of interactivity. This is well suited for batch jobs.
SCHED_IDLE: This is even weaker than nice 19, but its not a true idle timer scheduler in order to avoid to get into priority inversion problems which would deadlock the machine.
SCHED_FIFO/_RR are implemented in sched/rt.c and are as specified by POSIX.


yield_task(…)

This function is basically just a dequeue followed by an enqueue, unless the compat_yield sysctl is turned on; in that case, it places the scheduling entity at the right-most end of the red-black tree.

Wikipedia
=========

https://en.wikipedia.org/wiki/Completely_Fair_Scheduler

CFS is an implementation of a well-studied, classic scheduling algorithm called weighted fair queuing.[4]

opensource.com
==============

https://opensource.com/article/19/2/fair-scheduling-linux

The CFS scheduler has a target latency, which is the minimum amount of time—idealized to an infinitely small duration—required for every runnable task to get at least one turn on the processor.

Each runnable task then gets a 1/N slice of the target latency, where N is the number of tasks [...] and the default approximation is 20ms. The 1/N slice is, indeed, a timeslice—but not a fixed one [...] a low-priority nice value means that only some fraction of the 1/N slice is given to a task, whereas a high-priority nice value means that a proportionately greater fraction of the 1/N slice is given to a task

there is a minimum amount of time (with a typical setting of 1ms to 4ms) that any scheduled process must run before being preempted. This minimum is known as the minimum granularity.

Accordingly, once a task gets the processor, it runs for its entire weighted 1/N slice before being preempted in favor of some other task

StackExchange
=============

I think that this answer is not correct:

https://unix.stackexchange.com/questions/452172/does-linux-3-10-cfs-have-timeslices

The user points to two fomulas to calcualte a timeslice, but only the first one calculates a timeslice, the second accounts for the total amount of time needed for all threads to have a change to run. 
