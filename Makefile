
all: image

image:
	sudo docker build --tag aleixrocks/gitlab-jekyll-rst:1.0 .

run: image
	#sudo docker run --network host --name web aleixrocks/gitlab-jekyll-rst:1.0
	sudo docker run --net host --name web aleixrocks/gitlab-jekyll-rst:1.0
#	docker run --publish 4001:4000 --detach --name web gitlab-web:1.0
#	docker run gitlab-web:1.0
#	docker run --publish 8000:8080 --detach --name bb bulletinboard:1.0
#
clean:
	sudo docker rm --force web
