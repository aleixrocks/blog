---
layout: post
title:  "This is RST!"
date:   2020-03-17 00:32:14 -0300
categories: jekyll update
---

Pipaspalpajaro

 .. code-block:: ruby
 
    # Output "I love ReST"
    say = "I love ReST"
    puts say

The area of a circle is :math:`A_\text{c} = (\pi/4) d^2`.

 .. code-block:: console
    :caption: Read Hacker News on a budget
    :url: http://news.ycombinator.com
    :title: Hacker News
 
    $ curl http://news.ycombinator.com | less


=================
My Project Readme
=================
-------------------------
Clever subtitle goes here
-------------------------

Introduction
============

This is an example reStructuredText document that starts at the very top
with a title and a sub-title. There is one primary header, Introduction.
There is one example subheading below.
The document is just plain text so it is easily readable even before
being converted to HTML, man page, PDF or other formats.

Subheading
----------

The basic syntax is not that different from Markdown, but it also
has many more powerful features that Markdown doesn't have. We aren't
taking advantage of those yet though.

- Bullet points
- Are intuitive
- And simple too

